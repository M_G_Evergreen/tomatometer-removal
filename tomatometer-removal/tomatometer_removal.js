const BLURB_CUTOFF = 280;
function get_user_opinion()
{
	let opinions = document.getElementsByClassName("mop-audience-reviews__reviews-wrap")[0];
	if (!opinions || opinions.children.length == 0)
	{
		return "No reviews yet."
	}
	opinion_no = Math.floor(Math.random() * opinions.children.length);
	review = opinions.children[opinion_no].children[0].children[1].innerText;
	if (!review.endsWith(".") && !review.endsWith("!") && !review.endsWith("?"))
	{
		review = review + ".";
	}
	if (review.length > BLURB_CUTOFF)
	{
		review = review.substr(0, BLURB_CUTOFF) + "...";
	}
	return review;
}

// Remove the Tomatometer on /m/ pages
let tomatometer_half = document.getElementsByClassName("mop-ratings-wrap__half")[0];
if (tomatometer_half)
{
	tomatometer_half.parentNode.removeChild(tomatometer_half);
}

// Replace "Critics Consensus" with "A Viewer's Opinion"
let wrap_info = document.getElementsByClassName("mop-ratings-wrap__info")[0];
if (wrap_info)
{
	wrap_info.children[0].innerText = "A Viewer's Opinion";
	wrap_info.children[1].innerText = get_user_opinion();
}

// Remove the Tomatometer on /tv/ pages
var critic_score;
while (critic_score = document.getElementsByClassName("critic-score")[0])
{
	critic_score.parentNode.removeChild(critic_score);
}

// Remove little tomatoes on the home page
var tomatometer;
while (tomatometer = document.getElementsByClassName("media-list-item-tomatometer")[0])
{
        tomatometer.parentNode.removeChild(tomatometer);
}
while (tomatometer = document.getElementsByClassName("dynamic-text-list__tomatometer-group")[0])
{
        tomatometer.parentNode.removeChild(tomatometer);
}
while (tomatometer = document.getElementsByTagName("score-icon-critic")[0])
{
        tomatometer.parentNode.removeChild(tomatometer);
}

// Replace them with the audience score
for (e of document.getElementsByClassName("dynamic-text-list__audienceScore-group"))
{
	e.style = "";
}

for (e of document.getElementsByClassName("media-list-item-audience"))
{
	e.style = "";
}

// Remove the Critic Reviews
let contentReviews = document.getElementById("contentReviews");
if (contentReviews)
{
	contentReviews.parentNode.removeChild(contentReviews);
}

// Remove Certified Fresh Picks
let certified_fresh_picks = document.getElementById("certified-fresh-picks");
if (certified_fresh_picks)
{
	certified_fresh_picks.parentNode.removeChild(certified_fresh_picks);
}


// Remove sidebar tomatoes
for (e of document.getElementsByClassName("sidebarInTheaterOpening"))
{
	var left_col;
	while (left_col = e.getElementsByClassName("left_col")[0])
	{
		e.removeChild(left_col);
	}
}
for (e of document.getElementsByClassName("tv_show_tr"))
{
	var left_col;
	while (left_col = e.getElementsByClassName("left_col")[0])
	{
		e.removeChild(left_col);
	}
}


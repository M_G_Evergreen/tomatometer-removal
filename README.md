# Rotten Tomatoes critic remover

![Logo](tomatometer_goog_promo_3.png)
The scores put onto Rotten Tomatoes are not representative of whether or not the content is likable or not.
I set out to change that.

# Examples

![Example 1](tomatometer_screen_1.png)
_Adjusts the rating of the movie to a more accurate one_

![Example 2](tomatometer_screen_2.png)
_Provides a more accurate estimate of how watchable a given movie is_

![Example 3](tomatometer_screen_3.png)
_Deep integration with the site removes almost all traces of editorial opinion_

# Download
[Download it for **Firefox**](https://addons.mozilla.org/en-US/firefox/addon/rotten-tomatoes-critic-remover)

[Download it for **Chrome**](https://chrome.google.com/webstore/detail/gkihndnbignogceocldgfgemmofeccfp)

Have I missed a browser? Open an issue, it will be handled.

# Guarantees

I won't steal your data or make your computer behave in unexpected ways.
Feel free to check the source code out, it's simple and understandable.
